To change mortalilty/vaccination indicator one may need to change corresponding variables in config.py.

To run the 'pipeline' after parameters change it is neccessary to run notebooks in exact order:
 - EDA.ipynb
 - Countries clustering.ipynb
 - Countries comparison.ipynb
 - Suitable countries for comparison selection.ipynb
 