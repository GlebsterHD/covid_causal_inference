from functools import partial
from src.visualization.plot_saving_utils import get_save_path

MORTALITY_INDICATOR = 'excess_mortality'
VACCINATION_INDICATOR = 'total_vaccinations_per_hundred'

get_pic_save_path = partial(
    get_save_path,
    mort_indicator=MORTALITY_INDICATOR,
    vac_indicator=VACCINATION_INDICATOR,
)
