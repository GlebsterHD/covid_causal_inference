import pandas as pd
from pathlib import Path


DATA_URL = "https://covid.ourworldindata.org/data/owid-covid-data.xlsx"
FINAL_DATE = '2022-09-15'
owid_covid_data_folder = Path.cwd().parents[1] / 'data' / 'external'


def download_and_save_covid_data():
    """Downloads dataset and cuts it to final_date."""
    df = pd.read_excel(DATA_URL)
    df = df[df['date'] <= FINAL_DATE]
    df.to_csv(owid_covid_data_folder / "owid-covid-data.csv")


if __name__ == '__main__':
    download_and_save_covid_data()
    print('Data was downloaded successfully.')
