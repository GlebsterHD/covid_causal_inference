import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


from src.features.covid_spread_utils import (
    get_covid_spread_rate,
)
from src.features.comparison_utils import (
    SHORTCUTS,
)

from src.visualization.plot_saving_utils import (
    save_graph,
)


def plot_indicator_kde(
    covid_df: pd.DataFrame,
    indicator: str,
    iso_codes_for_subset: list,
    save_plot: bool = False,
) -> None:
    """
    Plots distribution of indicator for all data (World data)
    and subset with iso_codes included in iso_codes_for_subset.
    If save_plot is True, saves plot in the path:
    'reports/pictures_for_presentation/indicator_kde/{indicator}'

    :param covid_df: DataFrame with covid data
    :param indicator: well-beeng indicator
    :param iso_codes_for_subset: subset of chosen iso_codes to compare
        with all data
    :param save_plot: save plot or not
    """
    plt.figure(figsize=(7, 7))

    # World Data
    sns.kdeplot(
        x=covid_df.groupby(['iso_code'])[indicator].first(),
    )
    # Subset data
    sns.kdeplot(
        x=covid_df[
            covid_df.iso_code.isin(iso_codes_for_subset)
        ].groupby(['iso_code'])[indicator].first(),
    )

    plt.legend(labels=["World data", "Subset data"])
    if save_plot:
        save_graph(
            plot_dir='indicator_kde',
            plot_name=f'/{indicator}',
        )


def plot_lambda_for_country(
    covid_df: pd.DataFrame,
    iso_code: str,
    save_plot: bool = False,
) -> None:
    """
    Plots covid lambda vs time for selected country (iso_code).
    Paints lambda monotonous regions in different colors.
    If save_plot is True, saves plot in the path:
    'reports/pictures_for_presentation/lambda_plots/{iso_code}'

    :param covid_df: DataFrame with covid data
    :param iso_code: iso_code for selected country
    :param save_plot: save plot or not
    """

    df = get_covid_spread_rate(covid_df, iso_code)
    plt.figure(figsize=(7, 7),)
    sns.lineplot(
        data=df,
        x='spread_time',
        y='covid_lambda',
        hue='monotonic_region',
    )
    plt.xlabel('spread_time', size=13,)
    plt.ylabel('covid_lambda', size=13,)
    plt.title(f'covid_lambda for {iso_code}')
    if save_plot:
        save_graph(
            plot_dir='lambda_plots',
            plot_name=f'/{iso_code}',
        )


def plot_vaccination_for_country(
    covid_df: pd.DataFrame,
    iso_code: str,
    vaccination_indicator: str,
    save_plot: bool = False,
) -> None:
    """
    Plots number of people vaccinated per hundred vs time
    for selected country (iso_code).

    If save_plot is True, saves plot in the path:
    'reports/pictures_for_presentation/vaccination_plots/{iso_code}'

    :param covid_df: DataFrame with covid data
    :param iso_code: iso_code for selected country
    :param save_plot: save plot or not
    """
    df = covid_df[
        (covid_df.iso_code == iso_code) &
        (covid_df.date >= covid_df.vac_start_date)
    ].copy()

    plt.figure(figsize=(7, 7))
    plt.plot(
        df.date,
        df[vaccination_indicator],
    )
    plt.xlabel('date', size=13)
    plt.ylabel(f'{vaccination_indicator}', size=13,)
    plt.title(f'{vaccination_indicator} for {iso_code}')
    plt.xticks(rotation=45,)
    if save_plot:
        save_graph(
            plot_dir='vaccination_plots',
            plot_name=f'/{vaccination_indicator} for {iso_code}',
        )


def get_comparison_plots(
    covid_df: pd.DataFrame,
    iso_code_1: str,
    iso_code_2: str,
    vaccination_indicator: str,
    mort_indicators: str,
    similar_countries_dfs: dict,
    save_plot: bool = False,
):
    """
    Plots covid cases, number of people vaccinated per hundred,
    excess mortality vs time for selected countries.

    :param covid_df: DataFrame with covid data
    :param iso_code_1: iso_code for first selected country
    :param iso_code_2: iso_code for second selected country
    :param save_plot: save plot or not
    """
    n_rows = 2 + len(mort_indicators)
    fig, ax = plt.subplots(n_rows, 1, figsize=(10, 3*n_rows), sharex=True)

    countries_df = covid_df[
        covid_df['iso_code'].isin([iso_code_1, iso_code_2])
    ].copy()
    countries_df['log10(total_cases)'] = np.log10(countries_df['total_cases'])

    # covid plot
    sns.lineplot(
        data=countries_df,
        x='date',
        y='log10(total_cases)',
        hue='iso_code',
        ax=ax[0],
    )

    # vaccination plot
    vac_start = countries_df.vac_start_date.min()
    vaccination_df = countries_df[
        countries_df.date >= vac_start
    ]
    sns.lineplot(
        data=vaccination_df,
        x='date',
        y=vaccination_indicator,
        hue='iso_code',
        ax=ax[1],
    )
    for num, mort_indicator in enumerate(mort_indicators):
        # mortality plot
        sns.lineplot(
            data=countries_df,
            x='date',
            y=mort_indicator,
            hue='iso_code',
            ax=ax[2 + num],
        )

        delta_strain_start_date = pd.to_datetime('2021-10-01')
        ax[2 + num].axvline(delta_strain_start_date, color='r')
        ax[2 + num].axvline(pd.to_datetime(vac_start), color='g')
        all_countries_stats = similar_countries_dfs[f'{mort_indicator}_all_countries_stats']
        graph_stats = {}
        stats = [
            f'mean_{SHORTCUTS[mort_indicator]}_BV',
            f'mean_{SHORTCUTS[mort_indicator]}_AV_BDS',
        ]
        for idx, stat in enumerate(stats, 1):
            graph_stats[stat] = np.round(
                (all_countries_stats[all_countries_stats['iso_code'].eq(iso_code_1)][stat].values[0] -
                all_countries_stats[all_countries_stats['iso_code'].eq(iso_code_2)][stat].values[0]),
                2,
            )
            ax[2 + num].text(
                pd.to_datetime(vac_start) + (-1)**idx*np.timedelta64(150, 'D'),
                countries_df[mort_indicator].max() * 0.9,
                graph_stats[stat],
                {'fontsize': 15},
            )
        
    fig.suptitle(f'{iso_code_1} vs {iso_code_2}', size=15)
    plt.tight_layout()
    if save_plot:
        save_graph(
            plot_dir=f'comparison_plots/{vaccination_indicator} vs {mort_indicator}',
            plot_name=f'/{iso_code_1} vs {iso_code_2}',
        )


def plot_total_vs_week(
    df: pd.DataFrame,
    iso_code: 'str',
    save_plot: bool = False,
):
    """
    Plot total covid cases vs new cases smoothed per week in country

    If save_plot is True, saves plot in the path:
    'reports/pictures_for_presentation/total_vs_week/{iso_code}'

    :param covid_df: DataFrame with covid data
    :param iso_code: iso_code for selected country
    :param save_plot: save plot or not
    """
    country_df = df[df.iso_code == iso_code].copy()

    first_vac_index = country_df.people_vaccinated.isna()[
        ~country_df.people_vaccinated.isna()
    ].index[0]

    vac_start_date = country_df.loc[
        first_vac_index,
        'date',
    ]

    country_df['before_vaccination'] = country_df.date < vac_start_date
    country_df['total_cases'] = np.log(country_df['total_cases'])
    country_df['new_cases_smoothed'] = np.log(country_df['new_cases_smoothed'])

    sns.lineplot(
        x='total_cases',
        y='new_cases_smoothed',
        data=country_df,
        hue='before_vaccination',
    )

    plt.ylabel('new_cases_smoothed')
    plt.xlabel('total_cases')
    plt.title(iso_code)

    if save_plot:
        save_graph(
            plot_dir='total_vs_week',
            plot_name=f'/{iso_code}',
        )


def plot_effect_types(
    comparison_df: pd.DataFrame,
    effect_type: str,
    vaccination_indicator: str,
    mort_indicator: str,
    save_plot: bool = False,
):
    """
    Draws a graph of the share of vaccination effects for countries,
    presented in `comparison_df`.

    :param comparison_df: DataFrame with comparison data
    :param effect_type: what type to plot. Can be 'vaccination rate' or
        'vaccination'
    :param vac_indicator: vaccination indicator selected for DID
    :param mort_indicator: mortality indicator selected for DID
    :param save_plot: save plot or not
    """
    effect_types_columns = {
        'vaccination rate': 'VacRate_effect',
        'vaccination': 'Vac_effect',
    }

    effect_types_colors = {
        'bad': 'red',
        'good': 'green',
        'diff': 'grey',
        np.nan: 'black'
    }

    plt.figure(figsize=(10, 10))
    filtered_df = comparison_df.filter(
        like=effect_types_columns[effect_type],
    )

    filtered_df.apply(
        pd.Series.value_counts,
        dropna=False,
        normalize=True,
    ).T.plot(
        kind='barh',
        stacked=True,
        color=effect_types_colors,
    )
    plt.legend(bbox_to_anchor=(1.04, 1), loc="upper left")

    # title of plot
    where = 'chosen countries' if filtered_df.shape[0] < 35 else 'world data'
    plt.title(
        f"""{effect_type} effects for {vaccination_indicator}
        vs {mort_indicator} for {where}"""
    )

    if save_plot:
        save_graph(
            plot_dir=f'effect_plots/{effect_types_columns[effect_type]}',
            plot_name=f'{vaccination_indicator} vs {mort_indicator} for {where}',
        )
