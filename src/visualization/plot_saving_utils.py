import matplotlib.pyplot as plt
from pathlib import Path


SAVE_FOLDER = 'reports/pictures_for_presentation/'


def save_graph(
    plot_dir: str,
    plot_name: str,
) -> None:
    """
    Saves plot in the path:'SAVE_FOLDER/plot_dir/plot_name'

    :param plot_dir: plot folder name
    :param plot_name: plot file name
    """
    plt.tight_layout()
    save_folder = SAVE_FOLDER + plot_dir
    Path(save_folder).mkdir(parents=True, exist_ok=True)
    plt.savefig(
        save_folder + plot_name,
        transparent=True,
        dpi=500,
    )


def get_save_path(
    plot_type: str,
    mort_indicator: str,
    vac_indicator: str,
) -> str:
    """
    Returns path to save plot.

    :param plot_type: plot type
    :param mort_indicator: mortality indicator selected for DID
    :param vac_indicator: vaccination indicator selected for DID

    :return: plot save path
    """
    save_folder = SAVE_FOLDER + f'{plot_type}/'
    Path(save_folder).mkdir(parents=True, exist_ok=True)
    pic_name = f'{vac_indicator}_vs_{mort_indicator}'
    return save_folder + pic_name
