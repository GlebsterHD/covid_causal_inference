import pandas as pd
import numpy as np


def get_vaccination_rates(
    covid_df: pd.DataFrame,
    iso_code: str,
):
    """
    Calculates the average vaccination rate and the number
    of days up to 10, 15 and 50% of the population vaccinated.

    :param covid_df: DataFrame with covid data
    :param iso_code: iso_code for selected country

    :return: Dict with vaccination parameters
    """
    vac_data = covid_df[
        (covid_df.iso_code == iso_code) &
        (covid_df.date >= covid_df.vac_start_date)
    ].reset_index(drop=True)
    vac_start_date = vac_data.loc[0, 'date']

    vac_rates = {}
    for pct in [10, 25, 50]:
        if (vac_data.people_vaccinated_per_hundred > pct).any():
            # Date and number of days till pct% people got vaccinated
            vac_achieved_date = vac_data[
                vac_data['people_vaccinated_per_hundred'] > pct
            ].iloc[0].date
            vac_rates[f'days_to_{pct}%'] = (vac_achieved_date - vac_start_date).days
        else:
            vac_rates[f'days_to_{pct}%'] = np.nan
    vaccination_period = (vac_data.date.max() - vac_start_date).days
    vac_rates['mean_vac_rate'] = np.round(
        vac_data.people_vaccinated_per_hundred.max() / vaccination_period,
        4
    )
    return vac_rates
