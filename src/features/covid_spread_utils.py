import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from typing import List, Tuple


def get_monotonic_lambda_regions(
    lambda_diff_sign: pd.Series,
    start_index: int,
    treshhold: int = 6,
) -> List[int]:
    """
    Computes approximate number of covid lambda changes
    monotonic regions.

    :param lambda_diff_sign: lambda difference sign, computed vs previous
        lambda value
    :param start_index: index to start computing monotonic regions,
        necessary in order to cut off surges at the beginning of
        the covid spread
    :param treshhold: The number of records with a new sign, defining the
        beginning of a new monotonous section

    :return: List with  monotonous section numbers
    """
    plus_count = 0
    minus_count = 0
    monotonic_region_number = 1
    monotonic_ind_list = []

    for i in lambda_diff_sign[start_index:].index:
        if lambda_diff_sign[i] == -1:
            minus_count += 1
        else:
            plus_count += 1

        # See if the covid spread rate change seems monotonic
        change_region_condition = (
            (plus_count > treshhold) &
            (minus_count > treshhold) &
            ((plus_count > 10) | (minus_count > 10)) &
            (lambda_diff_sign[i - treshhold: i] == lambda_diff_sign[i]).all()
        )

        # Drop out random sign changes
        fluctuation_condition = (
            (max(plus_count, minus_count) > 10) &
            (min(plus_count, minus_count) < 2)
        )

        if fluctuation_condition:
            plus_count = 0
            minus_count = 0

        if change_region_condition:
            monotonic_region_number += 1
            plus_count = 0
            minus_count = 0
        monotonic_ind_list.append(monotonic_region_number)
    return monotonic_ind_list


def get_covid_spread_rate(
    covid_df: pd.DataFrame,
    iso_code: str,
) -> pd.DataFrame:
    """
    Returns covid_df for selected (iso_code) country with added
    spread rate characteristics columns.

    :param covid_df: DataFrame with covid data
    :param iso_code: iso_code for selected country

    :return: DataFrame with spread rate data
    """
    # Choose data for selected country with covid impact
    country_df = covid_df[covid_df['iso_code'] == iso_code].copy()
    country_df = country_df[
        country_df['date'] >= country_df['covid_start_date']
    ].reset_index(drop=True)

    # columns for lambda
    country_df['spread_time'] = (
        country_df['date'] - country_df['covid_start_date']
    ).dt.days
    country_df['log_N_cases'] = np.log(country_df['total_cases'])

    # lambda
    country_df['covid_lambda'] = country_df.log_N_cases / country_df.spread_time

    # check if covid spread_rate drops or raises
    country_df.loc[1:, 'lambda_diff'] = np.diff(country_df['covid_lambda'])
    country_df.loc[1:, 'lambda_diff_sign'] = np.sign(country_df.loc[1:, 'lambda_diff'])

    # define monotonic regions
    start_index = 6
    country_df.loc[start_index:, 'monotonic_region'] = get_monotonic_lambda_regions(
        lambda_diff_sign=country_df.lambda_diff_sign,
        start_index=start_index,
    )

    # define monotonic regions quantity
    country_df['n_monotonic_region'] = country_df['monotonic_region'].max()
    return country_df


def get_series_max_corr(
    df_1: pd.DataFrame,
    df_2: pd.DataFrame,
    time_shift: int,
) -> Tuple[int, float]:
    """
    Finds the time lag that maximizes the correlation coefficient of the time series,
    taking into account differences in the dates.

    :param df_1: DataFrame with covid data for first country
    :param df_2: DataFrame with covid data for first country
    :param time_shift: correction due to the covid start difference

    :return: lag and correlation coefficient
    """
    # To check the date shift around the covid start point in both countries,
    # we add the correction due to the covid start difference
    n_days = 60
    shift_start = time_shift - n_days
    shift_end = time_shift + n_days

    # We consider the correlation of the derivative,
    # i.e. synchronism changes for chosen `covid_column`
    corr_coefs = [
        df_1.diff().corr(df_2.diff().shift(lag))
        for lag in range(shift_start, shift_end)
    ]
    return np.argmax(corr_coefs) + shift_start, max(corr_coefs)


def get_max_covid_corr(
    covid_df: pd.DataFrame,
    covid_column: str,
    iso_code_1: str,
    iso_code_2: str,
    before_vac: bool = True,
    check_graph: bool = False,
) -> Tuple[int, float]:
    """
    Finds the time lag that maximizes the correlation coefficient of the time series
    of two countries, taking into account differences in the dates of the onset of
    the covid epidemic in countries. Gaps in values are filled by linear extrapolation.

    :param covid_df: DataFrame with covid data
    :param covid_column: indicator for countries comparison
    :param iso_code_1: iso_code for first selected country
    :param iso_code_2: iso_code for second selected country
    :param before_vac: if True, checks correlation for time series before vaccination,
        else for all data
    :param check_graph: if True, plots the graphs of `covid_column` for both countries
        with found lag

    :return: lag and correlation coefficient
    """
    df = covid_df.copy()
    data = {}
    for country_num, iso_code in enumerate([iso_code_1, iso_code_2]):
        df_condition = (df['iso_code'] == iso_code)
        if before_vac:
            # Vaccination start date
            vac_start_date = df[df_condition][
                'vac_start_date'
            ].values[0].astype('datetime64[D]')

            # Updated condition for time series
            df_condition = (
                (df['iso_code'] == iso_code) &
                (df['date'] < vac_start_date)
            )

        data[f'covid_start_date_{country_num}'] = df[df_condition][
            'covid_start_date'
        ].values[0].astype('datetime64[D]')

        # Get interpolated time series
        resampled_data = df[df_condition].resample('D', on='date')[[covid_column]]
        data[f'country_{country_num}_series'] = resampled_data.first().interpolate(
            inplace=False,
            limit_direction='forward',
            limit=100,
        )[covid_column]

    delta_covid_start = int(
        (data['covid_start_date_0'] - data['covid_start_date_1']) /
        np.timedelta64(1, 'D')
    )

    max_corr_lag, max_corr = get_series_max_corr(
        data['country_0_series'],
        data['country_1_series'],
        delta_covid_start
    )

    if check_graph:
        print(f'{covid_column}:')
        print(f'COVID start date for {iso_code_1} - {data["covid_start_date_0"]}')
        print(f'COVID start date for {iso_code_2} - {data["covid_start_date_1"]}')
        print(f'delta_covid_start = {delta_covid_start} days')
        print(f'Lag maximizing correlation is {max_corr_lag} days for {iso_code_2}')
        print(f'Max correlation is {max_corr:.4f}')
        print('-----------------------------------\n')

        fig, ax = plt.subplots()
        data['country_0_series'].plot(ax=ax)
        data['country_1_series'].shift(max_corr_lag).plot(ax=ax)
        ax.set_xlabel('date')
        ax.set_ylabel(f'{covid_column}')
        plt.legend([iso_code_1, iso_code_2])
    return max_corr_lag, max_corr
