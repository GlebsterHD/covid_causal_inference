from typing import Dict, Union
import pandas as pd
import numpy as np
import re

from scipy.stats import boxcox
from sklearn import preprocessing

from src.features.covid_spread_utils import (
    get_covid_spread_rate,
    get_series_max_corr,
)

from src.features.vaccination_utils import (
    get_vaccination_rates,
)

SHORTCUTS = {
    'excess_mortality': 'EM',
    'excess_mortality_cumulative': 'EMC',
    'excess_mortality_cumulative_absolute': 'EMCA',
    'excess_mortality_cumulative_per_million': 'EMCPM',
    'new_cases_smoothed_per_million': 'NCSPM',
    'new_deaths': 'ND',
    'new_deaths_per_million': 'NDPM',
    'new_deaths_smoothed': 'NDS',
    'new_deaths_smoothed_per_million': 'NDSPM',
    'total_deaths': 'TD',
    'total_deaths_per_million': 'TDPM',
}


def get_country_statistics(
    iso_code: str,
    covid_df: pd.DataFrame,
    mortality_indicator: str,
) -> Dict[str, Union[int, float, pd.Series]]:
    """
    Calculates the country covid and vaccination parameters
    required for pairwise comparisons.

    :param iso_code: iso_code for selected country
    :param covid_df: DataFrame with covid data
    :param mortality_indicator: mortality indicator selected for DID

    :return: Dict with country parameters
    """
    country_stats = {}
    country_df = covid_df[covid_df['iso_code'] == iso_code]
    country_stats['iso_code'] = iso_code
    country_stats['cluster'] = country_df.cluster.unique()[0]

    # covid_start_date
    country_stats['covid_start_date'] = country_df.covid_start_date.unique()[0]

    # vac_start_date
    country_stats['vac_start_date'] = country_df.vac_start_date.unique()[0]

    # days_covid_wo_vac
    country_stats['days_covid_wo_vac'] = (
        country_stats['vac_start_date'] - country_stats['covid_start_date']
    ).astype('timedelta64[D]') / np.timedelta64(1, 'D')

    # corr_quadrant
    country_stats['corr_quadrant'] = country_df.corr_quadrant.unique()[0]

    # continent
    country_stats['continent'] = country_df.continent.unique()[0]

    # covid_spread_rate
    covid_spread_rate = get_covid_spread_rate(covid_df, iso_code)
    country_stats['n_monotonic_region'] = covid_spread_rate.n_monotonic_region.values[0]
    country_stats['mean_lambda'] = np.ma.masked_invalid(
        covid_spread_rate['covid_lambda']
    ).mean()

    # Time series for correlation
    df_condition = (country_df['date'] < country_stats['vac_start_date'])
    for timeseries in ['new_cases_smoothed_per_million', mortality_indicator]:
        param_name = SHORTCUTS[timeseries] + '_corr_series'
        resampled_data = country_df[df_condition].resample('D', on='date',)[[timeseries]]
        country_stats[param_name] = resampled_data.first().interpolate(
            inplace=False,
            limit_direction='forward',
            limit=100,
        )[timeseries]

    # First 150 days of covid
    n_first_records = 150
    country_stats['first_lambda'] = covid_spread_rate.loc[
        :(n_first_records - 1),
        'covid_lambda',
    ].values

    # vac_spread_rate
    vac_spread_rate = get_vaccination_rates(
        covid_df,
        iso_code,
    )
    for rate in vac_spread_rate.keys():
        country_stats[f'{rate}'] = vac_spread_rate[rate]

    if mortality_indicator in SHORTCUTS.keys():
        shortcut = SHORTCUTS[mortality_indicator]
    else:
        shortcut = mortality_indicator
    country_stats[f'mean_{shortcut}'] = country_df[mortality_indicator].mean()

    # Mean mortality indicator before vaccination (BV)
    country_stats[f'mean_{shortcut}_BV'] = country_df[
        country_df['date'] < country_stats['vac_start_date']
    ][mortality_indicator].mean()

    # Mean mortality indicator after vaccination (AV)
    country_stats[f'mean_{shortcut}_AV'] = country_df[
        country_df['date'] > country_stats['vac_start_date']
    ][mortality_indicator].mean()

    # Mean mortality indicator after vaccination but before Delta-strain (BDS)
    delta_strain_start_date = '2021-10-01'
    country_stats[f'mean_{shortcut}_AV_BDS'] = country_df[
        (country_df['date'] > country_stats['vac_start_date']) &
        (country_df['date'] < delta_strain_start_date)
    ][mortality_indicator].mean()

    # Difference in mean mortality indicator after - before vaccination
    country_stats[f'diff_{shortcut}_AV-BV'] = (
        country_stats[f'mean_{shortcut}_AV'] -
        country_stats[f'mean_{shortcut}_BV']
    )
    # Difference in mean mortality indicator before Delta-strain - before vaccination
    country_stats[f'diff_{shortcut}_AV_BDS-BV'] = (
        country_stats[f'mean_{shortcut}_AV_BDS'] -
        country_stats[f'mean_{shortcut}_BV']
    )
    for averaging_interval in [90, 120, 150]:
        # Mean mortality indicator in N days before vaccination (bv)
        date_bv = (
            country_stats['vac_start_date'] -
            np.timedelta64(averaging_interval, 'D')
        )
        country_stats[f'mean_{shortcut}_{averaging_interval}d_BV'] = country_df[
            (country_df['date'] > date_bv) &
            (country_df['date'] < country_stats['vac_start_date'])
        ][mortality_indicator].mean()

        for vac_pct in [10, 25, 50]:
            window_size = np.timedelta64(averaging_interval, 'D')
            if np.isnan(vac_spread_rate[f'days_to_{vac_pct}%']):
                continue
            pct_vaccinated_date = country_stats['vac_start_date'] + np.timedelta64(
                int(vac_spread_rate[f'days_to_{vac_pct}%']), 'D'
            )

            # Mean mortality indicator in N days after vac_pct% vaccinated
            country_stats[
                f'mean_{shortcut}_{averaging_interval}d_after_{vac_pct}%_vac'
            ] = country_df[
                (country_df['date'] > pct_vaccinated_date) &
                (country_df['date'] < pct_vaccinated_date + window_size)
            ][mortality_indicator].mean()

            # Difference in mean mortality indicator after vac_pct% vaccinated
            # and N days before vaccination. Less is better (cause its mortality)
            country_stats[f'diff_{vac_pct}%_vac-{averaging_interval}d_BV'] = (
                country_stats[
                    f'mean_{shortcut}_{averaging_interval}d_after_{vac_pct}%_vac'
                ] - country_stats[f'mean_{shortcut}_{averaging_interval}d_BV']
            )
    return country_stats


def get_effect_type(
    value_1: float,
    value_2: float,
) -> str:
    """
    Returns the effect of the vaccination value or rate.
    """
    if (value_1 < 0) & (value_2 < 0):
        return 'good'
    elif (value_1 > 0) & (value_2 > 0):
        return 'bad'
    else:
        return 'diff'


def compare_two_countries(
    iso_code_1: str,
    iso_code_2: str,
    full_stats_df: pd.DataFrame,
) -> Dict[str, Union[int, float]]:
    """
    Conducts a comparison of the two countries, calculating the
    difference in covid and vaccination rates and parameters.
    Calculates the effect of vaccination rate and the effect
    of the share of the vaccinated population on mortality.

    :param iso_code_1: iso_code for first selected country
    :param iso_code_2: iso_code for second selected country
    :param full_stats_df: DataFrame with covid and vaccination
        parameters for all countries

    :return: Dict with comparison parameters
    """
    comparison_dict = {}
    comparison_dict['iso_code_1'] = iso_code_1
    comparison_dict['iso_code_2'] = iso_code_2

    df1 = full_stats_df[
        full_stats_df.iso_code == iso_code_1
    ].reset_index(drop=True).copy().loc[0]
    df2 = full_stats_df[
        full_stats_df.iso_code == iso_code_2
    ].reset_index(drop=True).copy().loc[0]

    for date in ['covid_start_date', 'vac_start_date']:
        comparison_dict[f'delta_{date}'] = (df1[date] - df2[date]).days

    for parameter in ['cluster', 'corr_quadrant', 'continent']:
        comparison_dict[f'same_{parameter}'] = (
            df1[parameter] == df2[parameter]
        )
    for parameter in ['n_monotonic_region', 'mean_lambda', 'mean_vac_rate']:
        comparison_dict[f'delta_{parameter}'] = (
            df1[parameter] - df2[parameter]
        )

    # Difference in first day's spread rate
    n_first_records = 150
    n_first_records_wo_vac = min(
        n_first_records,
        df1['days_covid_wo_vac'],
        df2['days_covid_wo_vac'],
    )

    delta_lambda_first_days = (
        df1['first_lambda'][1: n_first_records_wo_vac] -
        df2['first_lambda'][1: n_first_records_wo_vac]
    )

    comparison_dict['delta_first_lambda_lt_0.02'] = (
        delta_lambda_first_days < 0.02
    ).sum() / n_first_records_wo_vac

    for pct in [10, 25, 50]:
        # The sign is inverted because more days mean less vaccination rate
        comparison_dict[f'delta_days_to_{pct}%'] = (
           df2[f'days_to_{pct}%'] - df1[f'days_to_{pct}%']
        )

    # Compute optimal lag for max correlation of timeseries
    for timeseries in [val for val in full_stats_df.columns if '_corr_series' in val]:
        param_name = timeseries.split('_corr_series')[0]
        (comparison_dict[f'max_{param_name}_corr_lag'],
         comparison_dict[f'max_{param_name}_corr']) = get_series_max_corr(
            df_1=df1[timeseries],
            df_2=df2[timeseries],
            time_shift=comparison_dict['delta_covid_start_date'],
        )

    country_diff_columns = full_stats_df.filter(like='diff_').columns
    for diff_column in country_diff_columns:
        # Calculate the actual DID values
        did_column_name = diff_column.replace('diff_', 'DID_')
        comparison_dict[did_column_name] = (
            df1[diff_column] - df2[diff_column]
        )
        effect_did_sign = np.sign(comparison_dict[did_column_name])

        vac_rate_effect_column_name = diff_column.replace('diff_', 'VacRate_effect_')
        vac_effect_column_name = diff_column.replace('diff_', 'Vac_effect_')
        if (np.isnan(df1[diff_column]) | np.isnan(df2[diff_column])):
            comparison_dict[vac_rate_effect_column_name] = np.nan
            comparison_dict[vac_effect_column_name] = np.nan
            continue

        # Vaccination Effects
        # We conclude that vaccination works if both countries started to die less
        comparison_dict[vac_effect_column_name] = get_effect_type(
            df1[diff_column],
            df2[diff_column],
        )

        # Check if it is vac% parameter
        ints_in_diff_column = re.findall(r'\d+', diff_column)
        if len(ints_in_diff_column) == 2:
            pct = int(ints_in_diff_column[0])
            rate_difference_sign = np.sign(comparison_dict[f'delta_days_to_{pct}%'])
        else:
            rate_difference_sign = np.sign(comparison_dict['delta_mean_vac_rate'])

        # Vaccination Rate Effects
        # If the rate_difference_sign is -1 it means that first country vaccinated slower.
        # If the effect_did_sign is +1 it means that first country died more.
        # So, slower vaccination implies more deaths, that is why we use '!='
        comparison_dict[vac_rate_effect_column_name] = (
            'good' if rate_difference_sign != effect_did_sign else 'bad'
        )
    return comparison_dict


def get_similar_pairs(
    comparison_df: pd.DataFrame,
    n_countries: float,
) -> pd.DataFrame:
    """
    Leaves the top `n_countries` for comparison based on a custom overall
    similarity distance.
    There are parameters that reflect the similarity of the course of covid
    and differences in the vaccination of countries. The overall similarity
    distance is calculated by the Box-Cox transform of the squared
    feature value, filling in the gaps with the median.
    For features of difference, the inverse square of the value is taken.
    Next comes the sum of all the transformed features to obtain the
    total distance.

    :param comparison_df: DataFrame with comparison data
    :param n_countries: number of most similar countries left
    :returns: DataFrame with most similar countries
    """
    difference_parameters = [
        'delta_days_to_10%',
        'delta_days_to_25%',
        'delta_days_to_50%',
        'delta_mean_vac_rate',
    ]

    similarity_parameters = [
        'delta_first_lambda_lt_0.02',
        'delta_covid_start_date',
        'delta_mean_lambda',
    ]

    # Countries must be from the same well-being cluster
    df = comparison_df[comparison_df.same_cluster == True].copy()  # noqa:E712
    df['iso_codes'] = df['iso_code_1'] + '_' + df['iso_code_2']

    for parameter in similarity_parameters + difference_parameters:
        col_name = 'transformed_' + parameter
        # Fill in the gaps with the median
        df[parameter] = df[parameter].fillna(df[parameter].median())

        if parameter in difference_parameters:
            # Let's take the inverse value as an indicator of similarity
            df[col_name], best_lambda = boxcox(1 / (df[parameter] ** 2 + 1e-15))
        else:
            df[col_name], best_lambda = boxcox(df[parameter] ** 2 + 1e-15)

        df[col_name] = df[col_name] / df[col_name].max()
        df[col_name] = preprocessing.scale(df[col_name])

    df['distance'] = df.filter(like='transformed_').sum(axis=1)
    return df.sort_values('distance')[:n_countries]
