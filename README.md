# COVID_causal_inference

This repository presents a study of the causal relationship between vaccination and covid-19 mortality.
Cause-and-effect relationships were assessed using a difference-of-differences approach.

Based on the results of the work, we made a presentation at the DataFest 3.0 (Reliable ML) conference,
You can watch the performance here: https://www.youtube.com/watch?v=GsW6E1Emg20 (in Russian).

## Brief summary of the work:
 - An exploratory analysis of data from the original dataset with covid is presented
 - The main indicators of vaccination and incidence of covid in countries have been calculated
 - Comparative characteristics were calculated in pairwise comparison of countries
 - two approaches to the selection of eligible DID pairs of countries have been considered:
     - custom measure of distance based on covid
     - maximizing the correlation of covid and mortality trends

## Contributors
We conducted our research under the auspices of ODS Lab. Two people were involved in this project:

Komarov Ivan:
telegram: https://t.me/komarovcat

Sosnin Gleb:
telegram: https://t.me/GlebSosnin